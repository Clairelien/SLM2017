package tw.teddysoft.bdd.domain.invoice;

/**
 * Created by teddy on 2017/3/9.
 */
public class InvoiceBuilder {

    private double vatRate = 0.0;
    private int taxIncludedPrice = 0;
   private  int taxExcludedPrice=0;
    private InvoiceBuilder(){}

    public static InvoiceBuilder newInstance(){
        return new InvoiceBuilder();
    }

    public InvoiceBuilder withVatRate(double vatRate) {
        this.vatRate = vatRate;
        return this;
    }

    public InvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice) {
        this.taxIncludedPrice = taxIncludedPrice;
        this.taxExcludedPrice = InvoiceCalculator.getTaxExcludedPrice( taxIncludedPrice, this.vatRate ) ;
        return this;
    }

    public Invoice issue() {
        return new Invoice(taxIncludedPrice, vatRate, taxExcludedPrice
                , InvoiceCalculator.getVAT(taxIncludedPrice, vatRate));
    }

    public InvoiceBuilder withTaxExcludedPrice(int taxExcludedPrice) {
        this.taxExcludedPrice=taxExcludedPrice;
        this.taxIncludedPrice = InvoiceCalculator.getTaxIncludedPrice( taxExcludedPrice, this.vatRate ) ;
        return this;
    }
}
