package tw.teddysoft.bdd.domain.invoice;

import tw.teddysoft.bdd.domain.invoice.Invoice;
import tw.teddysoft.bdd.domain.invoice.InvoiceBuilder;

import java.util.Scanner;

/**
 * Created by Claire on 2017/3/23.
 */
public class main {

    public static void main(String[] args) {
        InvoiceBuilder builder ;
        Invoice invoice ;

        builder = InvoiceBuilder.newInstance();
        for( int i = 0 ; i < args.length ; i++ ) {
            switch ( args[i] ) {
                case "-r":
                    builder.withVatRate(Double.parseDouble(args[i+1]));
                    break;
                case "-i":
                    builder.withTaxIncludedPrice(Integer.parseInt(args[i+1]));
                    break;
                case "-e":
                    builder.withTaxExcludedPrice(Integer.parseInt(args[i+1]));
                    break;
                case "-h":
                    System.out.println("Issuing an invoice :\njava -jar CucumberBasic-1.0-SNAPSHOT-jar-with-dependencies.jar -r <vatRate> -i <taxIncludedPrice> -e <taxExcludedPrice>");
                    System.out.println("see help : \njava -jar CucumberBasic-1.0-SNAPSHOT-jar-with-dependencies.jar -h");
                    break;
            }
        }

        invoice = builder.issue() ;
        System.out.println("vat rate = "+invoice.getVatRate());
        System.out.println("vat = "+invoice.getVAT());
        System.out.println("tax included price = "+invoice.getTaxIncludedPrice());
        System.out.println("tax excluded price = "+invoice.getTaxExcludedPrice());
        //Scanner inputScanner = new Scanner(System.in);
//        for ( String s : args ) {
//            System.out.println(s);
//        }

//        System.out.println("Enter \"\"Exit\"\" to close the program.\n The VAT rate is : ");
//        Scanner inputScanner = new Scanner(System.in);
//        String userInput = null ;
//
//
//        while( !userInput.equals("Exit") ) {
//            userInput = inputScanner.nextLine() ;
//            vatRate = Double.parseDouble( userInput ) ;
//            builder = InvoiceBuilder.newInstance() ;
//            builder.withVatRate(vatRate) ;
//        }
//        System.out.println("***" + userInput + "***");
    }
}
