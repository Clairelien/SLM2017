package tw.teddysoft.bdd.domain.invoice;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by teddy on 2017/3/16.
 */
public class InvoiceCalculatorTest {

    //TODO: Rename the test case to reveal its intent
    @Test
    public void test_that_given_taxIncludePrice_is_10_get_zero_vat (){ // the test i rename
        assertEquals(0,InvoiceCalculator.getVAT(10,0.05));
    }

    @Test
    public void test_that_given_taxExcludePrice_is_10_get_11_taxExcludePrice (){ // the test i add for test getTaxIncludedPrice
        assertEquals(11,InvoiceCalculator.getTaxIncludedPrice(10,0.05));
    }
}
